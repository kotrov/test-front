enum Actions {
  REQUEST_USERS = 'requestUsers',
  UPDATE_USER = 'updateUser',
  CREATE_USER = 'createUser',
}

enum Mutations {
  SET_USERS = 'setUsers',
}

enum Getters {
  GET_USERS = 'getUsers',
}

export { Actions, Mutations, Getters };
