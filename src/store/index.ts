import { createStore } from "vuex";
import { config } from "vuex-module-decorators";

import UsersModule from "@/store/modules/UsersModule";

config.rawError = true;

const store = createStore({
  modules: {
    UsersModule,
  },
});

export default store;
