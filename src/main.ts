import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue3 from 'bootstrap-vue-3'
import store from "./store";
import axios from 'axios'
import VueAxios from 'vue-axios'

import './assets/main.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'

const app = createApp(App)

// todo get from env
axios.defaults.baseURL = 'https://localhost/';
axios.defaults.headers.common['Content-Type'] = 'application/json';


app.use(router)
app.use(store)
app.use(BootstrapVue3)
app.use(VueAxios, axios)

app.mount('#app')
