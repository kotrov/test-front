import {Actions, Mutations, Getters} from "@/store/enums/StoreEnums";
import {Module, Action, Mutation, VuexModule} from "vuex-module-decorators";
import axios from "axios";
import store from "@/store";

export interface User {
  id: number;
  email: string;
  userName: string;
  password: string;
}

@Module
export default class UsersModule extends VuexModule {
  users = <Array<User>>[]

  get [Getters.GET_USERS]() {
    return this.users;
  }

  @Mutation
  [Mutations.SET_USERS](payload) {
    this.users = payload
  }

  @Action
  [Actions.REQUEST_USERS](payload) {
    axios.get("users", {params: payload})
        .then(({data}) => {
          this.context.commit(Mutations.SET_USERS, data);
        }).catch(({response}) => alert('error'));
  }

  @Action
  [Actions.UPDATE_USER](payload) {
    axios.post("users/" + payload.id, payload).catch(({response}) => alert('error'));
  }

  @Action
  [Actions.CREATE_USER](payload) {
    axios.post("users", payload).catch(({response}) => alert('error'));

    store.dispatch(Actions.REQUEST_USERS, {});
  }
}
